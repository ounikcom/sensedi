<?php include('inc/doctype.php'); ?>
<?php

    $no_colaborador = 0;
    if ($_POST['user'] != '' && $_POST['psw'] != '') {
        $no_colaborador = 1;
    }

?>
    <head>

        <!-- title
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <title><?=_I("head_title_index", "Servicios Técnicos, Periciales y Mantenimiento de Inmuebles")?></title>


        <!-- metas
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <meta name="description" content="<?=_I("head_metas_home_description", "En Sensedi ofrecemos las mejores soluciones para servicios técnicos, periciales y mantenimiento de inmuebles, usando la más avanzada tecnología e innovación.")?>" />
        <meta name="keywords" content="<?=_I("head_metas_home_keywords", "Servicios técnicos, servicios periciales, mantenimiento de inmuebles, mantenimiento de edificios, rehabilitación de edificios, reforma de edificios, rehabilitación de viviendas")?>" />
        <?php include('inc/general-metas.php'); ?>


        <!-- css
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/css.php'); ?>


        <!-- favicon
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/favicon.php'); ?>


        <!-- scripts
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/scripts.php'); ?>

        <script type="text/javascript">

            function chk_frm_qhdf() {

                $('#nohay').slideToggle('fast');

                if ($("#user").val() == "") {
                    $("#frmOK").hide();
                    $("#frmErr").show();
                    $("#user").focus();
                    return;
                }
                if ($("#psw").val() == "") {
                    $("#frmOK").hide();
                    $("#frmErr").show();
                    $("#psw").focus();
                    return;
                }

                $("#pepe").submit();

                $("#frmErr").hide();
                $("#pepe").hide();
                $("#frmOK").show();

            }

        </script>


        <!-- script google analytics
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/google_analytics.php'); ?>


        <!-- popup mailchimp suscriber
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/mailchimp-popup-suscriber.php'); ?>


    </head>
    <body>

        <!-- menu
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <? include('inc/menu.php'); ?>


        <!-- colaboradores
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <div class="row">
            <div class="col_100 area_colaboradores" id="area_clientes">
                <div class="box back_extralightblue_trans">
                    <h1 class="tit bold"><?=_I("body_colaboradores", "ÁREA COLABORADORES")?></h1>
                    <div class="img"><img src="assets/img/imago-blanco.png" alt="Sensedi" /></div>
                    <form id="pepe" method="post" action="colaboradores">
                        <div class="frm">
                            <input type="text" name="user" id="user" placeholder="<?=_I("general", "nombre de usuario")?>" />
                            <input type="password" name="psw" id="psw" placeholder="<?=_I("general", "contraseña")?>" />
                        </div>
                        <a href="#" class="btn" onclick="chk_frm_qhdf()"><?=_I("body_clientes", "LOGIN")?></a>
                        <div id="frmErr" style="display:none"><?=_I("general", "Revisa los campos obligatorios por favor")?></div>
                        <div id="nohay" style="display:none"><?=_I("general", "No existen los datos introducidos")?></div>
                    </form>                    
                </div>
            </div>
        </div>


        <!-- footer
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/footer.php'); ?>


        <!-- copy
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/copy.php'); ?>


        <?php if ($no_colaborador == 1) { ?>
            <script type="text/javascript">
                $('#nohay').slideToggle('fast');    
            </script>
        <?php } ?>

    </body>
</html>