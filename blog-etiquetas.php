<?php include('inc/doctype.php'); ?>
<?php include("inc/wordpress.php"); ?>

<?
    function cortar_string ($string, $largo) { 
       $marca = "<!--corte-->"; 

       if (strlen($string) > $largo) { 
            
           $string = wordwrap($string, $largo, $marca); 
           $string = explode($marca, $string); 
           $string = $string[0]; 
       } 
       return $string; 

    } 

?>
    <head>

        <!-- title
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <title><?=_I("head_title_index", "Servicios Técnicos, Periciales y Mantenimiento de Inmuebles")?></title>


        <!-- metas
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <meta name="description" content="<?=_I("head_metas_home_description", "En Sensedi ofrecemos las mejores soluciones para servicios técnicos, periciales y mantenimiento de inmuebles, usando la más avanzada tecnología e innovación.")?>" />
        <meta name="keywords" content="<?=_I("head_metas_home_keywords", "Servicios técnicos, servicios periciales, mantenimiento de inmuebles, mantenimiento de edificios, rehabilitación de edificios, reforma de edificios, rehabilitación de viviendas")?>" />
        <?php include('inc/general-metas.php'); ?>


        <!-- css
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/css.php'); ?>


        <!-- favicon
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/favicon.php'); ?>


        <!-- scripts
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/scripts.php'); ?>


        <!-- script google analytics
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/google_analytics.php'); ?>


        <!-- popup mailchimp suscriber
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/mailchimp-popup-suscriber.php'); ?>


    </head>
    <body>

        <!-- menu
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <? include('inc/menu.php'); ?>

        <!-- blog
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <div class="row blog">

            <div class="col_66" style="margin:0px">

                <div class="content back_white">

            <?php
                $str_term_taxonomy_id = $_GET['id'];

                $query_name_taxo = "SELECT name FROM wp_terms WHERE term_id = ".$str_term_taxonomy_id;
                $result_name_taxo = $db_wp->Query($query_name_taxo); 
                while ($rst_name_taxo = $result_name_taxo->fetch(PDO::FETCH_ASSOC)) { 
                    $str_name_taxo = $rst_name_taxo['name'];
                }
                

            ?>

                    <div class="tit bold "><a href="/blog" class="bread">BLOG</a> > <?= $str_name_taxo ?></div>
                    <br><br>

            <?php

                $query_to_objects = "SELECT object_id 
                                     FROM wp_term_relationships 
                                     WHERE term_taxonomy_id = ".$str_term_taxonomy_id;

                $str_where_taxonomy = " AND ID IN (";

                $result_to_objects = $db_wp->Query($query_to_objects); 

                while ($rst_result_to_objects = $result_to_objects->fetch(PDO::FETCH_ASSOC)) { 
                
                    $str_where_taxonomy = $str_where_taxonomy . $rst_result_to_objects['object_id'] .",";
                
                }

                $str_where_taxonomy = substr($str_where_taxonomy, 0, strlen($str_where_taxonomy)-1 );

                $str_where_taxonomy = $str_where_taxonomy .") ";


                
                $stmt_contador = $db_wp->Query("

                    SELECT count(*) as cnt
                        FROM wp_posts WHERE post_status = 'publish' ".$str_where_taxonomy." ORDER BY post_date DESC, ID DESC 

                    ");

                $total_posts = $stmt_contador->fetch();
                $total_posts = $total_posts["cnt"];

                $num_registros_pag = 5;

                $paginas = $total_posts / $num_registros_pag;

                $limit = intval($_POST['pagina']) * $num_registros_pag;



                $noticia = array();
                $k = 0;

                $query_wp = "SELECT ID, post_title, post_content
                        FROM wp_posts WHERE post_status = 'publish' ".$str_where_taxonomy." ORDER BY post_date DESC, ID DESC LIMIT $limit, $num_registros_pag";

                $result = $db_wp->Query($query_wp); 
                while ($rst = $result->fetch(PDO::FETCH_ASSOC)) { 

                    $ruta_imagen = "";

                    $query1 = "select meta_value
                            from wp_postmeta where meta_key='_thumbnail_id' and post_id = " .$rst['ID'];

                    $result1 = $db_wp->Query($query1); 
                    $rst1 = $result1->fetch(PDO::FETCH_ASSOC);

                    if ($rst1 != "") {
                        $query2 = "select meta_value
                                from wp_postmeta where meta_key='_wp_attached_file' and post_id = " .$rst1['meta_value'];
                        $result2 = $db_wp->Query($query2); 
                        $rst2 = $result2->fetch(PDO::FETCH_ASSOC);

                        $ruta_imagen = "http://www.sensedi.com/admin/wp-content/uploads/".$rst2['meta_value'];
                    }

            ?>
                    <div class="post">

                        <? if ($ruta_imagen != "") { ?>
                        <div class="img_post" style="background-image: url('<?=$ruta_imagen?>');"></div>
                        <? } ?>
<!--                        <a href="blog-post.php?id=<?=$rst['ID']?>" title="" class="titulo"><?=$rst['post_title']?></a> -->

                        <a href="/blog/<? echo $rst['ID'] ?>/<? echo normalize_url($rst['post_title']) ?>" title="" class="titulo"> <h2><?=$rst['post_title']?></h2> </a>

<!--                        <a href="blog-categoria.php?c=<?=$rec["id"]?>" title="" class="categoria"><?= $s_cat ?></a> -->

                        <div class="etiquetas">

                            <?
                            $query_post_term = "select term_taxonomy_id from wp_term_relationships where object_id = ".$rst['ID'];

                            $result_post_term = $db_wp->Query($query_post_term); 
                            while ($rst_post_term = $result_post_term->fetch(PDO::FETCH_ASSOC)) { 

                                if ($rst_post_term['term_taxonomy_id'] != "1") {
                                    $query_term_item = "select * from wp_terms where term_id = ".$rst_post_term['term_taxonomy_id'];
                                    $result_term_item = $db_wp->Query($query_term_item); 
                                    $rst_term_item = $result_term_item->fetch(PDO::FETCH_ASSOC);
                                    echo "<a href='/etiquetas/".$rst_term_item['term_id']."/".normalize_url($rst_term_item['name'])."' title='".$rst_term_item['name']."'>".$rst_term_item['name']."</a>";
                                }

                            }
                            ?>
                        
                        </div>

                        <p class="color_grey">
                            <?=cortar_string($rst['post_content'], 300);?>... [+]
                        </p>
<!--                        <a href="blog-post.php?id=<?=$rst['ID']?>" class="btn"><?=_I("blog", "leer más")?></a> -->
                        <a href="/blog/<? echo $rst['ID'] ?>/<? echo normalize_url($rst['post_title']) ?>" title="" class="btn"> <?=_I("blog", "leer más")?> </a>

                    </div>


                <?php
                }
                ?>
                
                    <!-- blog paginación
                    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                    <?php include('inc/blog-paginacion.php'); ?>


                </div>


            </div>

            <div class="col_33">

                <!-- blog buscador
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/blog-buscador.php'); ?>


                <!-- mailchimp newsletter
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/mailchimp-newsletter.php'); ?>


                <!-- servicios técnicos
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/servicios-tecnicos.php'); ?>


                <!-- blog ultimas publicaciones
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/blog-ultimas.php'); ?>


                <!-- blog categorias
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!--                <?php include('inc/blog-categorias.php'); ?> -->


            </div>

        </div>


        <!-- footer
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/footer.php'); ?>


        <!-- copy
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/copy.php'); ?>


    </body>
</html>