<?php include('inc/doctype.php'); ?>
<?php include("inc/wordpress.php"); ?>

<?

    function cortar_string ($string, $largo) { 
       $marca = "<!--corte-->"; 

       if (strlen($string) > $largo) { 
            
           $string = wordwrap($string, $largo, $marca); 
           $string = explode($marca, $string); 
           $string = $string[0]; 
       } 
       return $string; 

    } 

?>

    <head>

        <!-- title
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <title><?=_I("head_title_index", "Servicios Técnicos, Periciales y Mantenimiento de Inmuebles")?></title>

        <!-- metas
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <meta name="description" content="<?=_I("head_metas_home_description", "En Sensedi ofrecemos las mejores soluciones para servicios técnicos, periciales y mantenimiento de inmuebles, usando la más avanzada tecnología e innovación.")?>" />
        <meta name="keywords" content="<?=_I("head_metas_home_keywords", "Servicios técnicos, servicios periciales, mantenimiento de inmuebles, mantenimiento de edificios, rehabilitación de edificios, reforma de edificios, rehabilitación de viviendas")?>" />
        <?php include('inc/general-metas.php'); ?>


        <!-- css
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/css.php'); ?>


        <!-- favicon
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/favicon.php'); ?>


        <!-- scripts
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/scripts.php'); ?>


        <!-- script google analytics
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/google_analytics.php'); ?>


        <!-- popup mailchimp suscriber
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/mailchimp-popup-suscriber.php'); ?>


    </head>
    <body>

        <!-- menu
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <? include('inc/menu.php'); ?>

        <?
            $str_search_for = $_POST['search_for'];
        ?>


        <!-- blog search
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <div class="row blog">

            <div class="col_66" style="margin:0px">

                <div class="content back_white">
                    <div class="tit bold "><a href="/blog" class="bread">BLOG</a> > <?=_I("blog", "Estamos buscando")?> "<?= $str_search_for ?>"</div>
                    <br><br>

                <?php

                    $stmt_contador = $db_wp->Query("

                        SELECT count(*) as cnt
                            FROM wp_posts 
                            WHERE post_status = 'publish' 
                            AND (post_title like '%".$str_search_for."%' or post_content like '%".$str_search_for."%') 
                            ORDER BY post_date DESC, ID DESC 

                        ");

                    $total_posts = $stmt_contador->fetch();
                    $total_posts = $total_posts["cnt"];

                    $num_registros_pag = 5;

                    $paginas = $total_posts / $num_registros_pag;

                    $limit = intval($_POST['pagina']) * $num_registros_pag;



                    $noticia = array();
                    $k = 0;

                    $query_wp = "
                            SELECT ID, post_title, post_content
                            FROM wp_posts 
                            WHERE post_status = 'publish' 
                            AND (post_title like '%".$str_search_for."%' or post_content like '%".$str_search_for."%') 
                            ORDER BY post_date DESC, ID DESC LIMIT $limit, $num_registros_pag
                            ";

                    $result = $db_wp->Query($query_wp); 
                    while ($rst = $result->fetch(PDO::FETCH_ASSOC)) { 

                        $ruta_imagen = "";

                        $query1 = "select meta_value
                                from wp_postmeta where meta_key='_thumbnail_id' and post_id = " .$rst['ID'];

                        $result1 = $db_wp->Query($query1); 
                        $rst1 = $result1->fetch(PDO::FETCH_ASSOC);

                        if ($rst1 != "") {
                            $query2 = "select meta_value
                                    from wp_postmeta where meta_key='_wp_attached_file' and post_id = " .$rst1['meta_value'];
                            $result2 = $db_wp->Query($query2); 
                            $rst2 = $result2->fetch(PDO::FETCH_ASSOC);

                            $ruta_imagen = "http://www.sensedi.com/admin/wp-content/uploads/".$rst2['meta_value'];
                        }

                ?>
                
                    <div class="post">

                        <? if ($ruta_imagen != "") { ?>
                        <div class="img_post" style="background-image: url('<?=$ruta_imagen?>');"></div>
                        <? } ?>
                        <a href="blog-post.php?id=<?=$rst['ID']?>" title="" class="titulo"><?=$rst['post_title']?></a>

                        <div class="etiquetas">

                            <?
                            $query_post_term = "select term_taxonomy_id from wp_term_relationships where object_id = ".$rst['ID'];

                            $result_post_term = $db_wp->Query($query_post_term); 
                            while ($rst_post_term = $result_post_term->fetch(PDO::FETCH_ASSOC)) { 

                                if ($rst_post_term['term_taxonomy_id'] != "1") {
                                    $query_term_item = "select * from wp_terms where term_id = ".$rst_post_term['term_taxonomy_id'];
                                    $result_term_item = $db_wp->Query($query_term_item); 
                                    $rst_term_item = $result_term_item->fetch(PDO::FETCH_ASSOC);
                                    echo "<a href='/etiquetas/".$rst_term_item['term_id']."/".normalize_url($rst_term_item['name'])."' title='".$rst_term_item['name']."'>".$rst_term_item['name']."</a>";
                                }

                            }
                            ?>
                        
                        </div>

                        <p class="color_grey">
                            <?=cortar_string($rst['post_content'], 300);?>... [+]
                        </p>
                        <a href="blog-post.php?id=<?=$rst['ID']?>" class="btn"><?=_I("blog", "leer más")?></a>

                    </div>

                <?php
                    $exist = true;                
                }

                if (!$exist) {
                ?>



                    <div class="post">

                        <a href="blog-post.php?id=<?=$rec["id_cont"]?>" title="" class="titulo">Upsss!</a>
                        <p class="color_grey">

                        <? if ($_SESSION['idioma_code'] == 'es') { ?>
                            <strong>Lo sentimos pero no hemos encontrado resultados para tu búsqueda.</strong>
                            <br><br>
                            Puedes volver a intentarlo haciendo menos restrictiva tu búsqueda, utilizando, por ejemplo, menos palabras o parte de ellas.
                            <br><br>
                            Si quieres estar informado de las publicaciones que hacemos, <strong>puedes suscribirte a nuestra newsletter</strong> para que te enviemos nuestro boletín periódicamente.
                            <br><br>

                        <? } ?>

                        <? if ($_SESSION['idioma_code'] == 'ca') { ?>
                            <strong> Ho sentim, no hem trobat resultats per la teva recerca. </strong>
                            <br><br>
                            Pots tornar a intentar-ho fent menys restrictiva la cerca, utilitzant, per exemple, menys paraules o part d'elles.
                            <br><br>
                            Si vols estar informat de les publicacions que fem, <strong> pots subscriure a la nostra newsletter </strong> perquè t'enviem el nostre butlletí periòdicament.
                            <br><br>

                        <? } ?>
                         
                        <? if ($_SESSION['idioma_code'] == 'en') { ?>
                            <strong>We are sorry but we did not find results for your search.</strong>
                            <br><br>
                            You can try again by making your search less restrictive, using, for example, fewer words or parts of them.
                            <br><br>
                            If you want to be informed of the publications we make, <strong> you can subscribe to our newsletter </strong> so that we send you our newsletter periodically.
                            <br><br>

                        <? } ?>

                        <? if ($_SESSION['idioma_code'] == 'fr') { ?>
                            <strong>Désolé, mais nous ne l'avons pas trouvé de résultats pour votre recherche.</strong>
                            <br><br>
                            Vous pouvez essayer de nouveau faire votre recherche moins restrictive en utilisant, par exemple, moins de mots ou de parties d'entre eux.
                            <br><br>
                            Si vous souhaitez être informé des publications que nous faisons, <strong> Vous pouvez vous abonner à notre newsletter </strong> afin que nous puissions envoyer notre bulletin d'information régulièrement.
                            <br><br>
                            
                        <? } ?>

                        </p>

                        <a href="blog.php" class="btn"><?=_I("blog", "Volver al blog")?></a>

                    </div>
                <?
                }
                ?>

                    <!-- blog paginación
                    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                    <?php include('inc/blog-paginacion.php'); ?>


                </div>

            </div>

            <div class="col_33">


                <!-- blog buscador
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/blog-buscador.php'); ?>


                <!-- mailchimp newsletter
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/mailchimp-newsletter.php'); ?>


                <!-- edificio
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/edificio.php'); ?>


                <!-- servicios técnicos
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/servicios-tecnicos.php'); ?>


            </div>

        </div>


        <!-- footer
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/footer.php'); ?>


        <!-- copy
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/copy.php'); ?>


    </body>
</html>