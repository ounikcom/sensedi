<?php include('inc/doctype.php'); ?>
    <head>

        <!-- title
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <title><?=_I("head_title_index", "Servicios Técnicos, Periciales y Mantenimiento de Inmuebles")?></title>


        <!-- metas
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <meta name="description" content="<?=_I("head_metas_home_description", "En Sensedi ofrecemos las mejores soluciones para servicios técnicos, periciales y mantenimiento de inmuebles, usando la más avanzada tecnología e innovación.")?>" />
        <meta name="keywords" content="<?=_I("head_metas_home_keywords", "Servicios técnicos, servicios periciales, mantenimiento de inmuebles, mantenimiento de edificios, rehabilitación de edificios, reforma de edificios, rehabilitación de viviendas")?>" />
        <?php include('inc/general-metas.php'); ?>


        <!-- css
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/css.php'); ?>


        <!-- favicon
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/favicon.php'); ?>


        <!-- scripts
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/scripts.php'); ?>


        <!-- script google analytics
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/google_analytics.php'); ?>


        <!-- popup mailchimp suscriber
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/mailchimp-popup-suscriber.php'); ?>


    </head>
    <body>

        <!-- menu
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <? include('inc/menu.php'); ?>

        <?
            $id_cat = $_GET['c'];

            $stmt = $db->Query("select * from t_categorias where id = $id_cat"); 

            while ($recC = $stmt->fetch(PDO::FETCH_ASSOC)) { 
                $s_cat = $recC["nombre"];
            }


        ?>


        <!-- que hacemos
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <div class="row blog">

            <div class="col_66" style="margin:0px">

                <div class="content back_white">
                    <div class="tit bold "><a href="/blog" class="bread">BLOG</a> > <?= $s_cat ?></div>
                    <br><br>

                <?

                $strsql = "SELECT t_categorias.id, t_contenidos.titulo as titulo, t_contenidos.descripcion as descripcion, t_categorias.imagen as imagen, t_contenidos.id_cont as id_cont, t_contenidos.imagen as imagen 
                            FROM t_contenidos 
                            INNER JOIN t_categorias ON t_categorias.id = t_contenidos.id_categoria 
                            INNER JOIN tv_pais ON t_contenidos.id_pais = tv_pais.id
                            INNER JOIN tv_idiomas ON t_contenidos.id_idioma = tv_idiomas.id
                            WHERE t_contenidos.activo='S' 
                            AND t_contenidos.id_seccion = 3
                            AND t_contenidos.deleted_at is NULL
                            AND t_categorias.id = $id_cat
                            AND tv_idiomas.abr = '" .$_SESSION['idioma_code']. "' 
                            AND tv_pais.code_pais = '" .$_SESSION['pais_code']. "' 
                            AND t_contenidos.titulo <> '' ";

                $stmt = $db->Query($strsql); 
                $exist = false;
                while ($rec = $stmt->fetch(PDO::FETCH_ASSOC)) { 
                    $imagen = 'data:image/jpeg' . $type . ';base64,' . base64_encode($rec["imagen"]);
                ?>

                    <div class="post">

                        <div class="img_post" style="background-image: url('<?=$imagen?>');"></div>
<!--                        <a href="blog-post.php?id=<?=$rec["id_cont"]?>" title="" class="titulo"><?=$rec["titulo"]?></a> -->
                        <a href="/blog/<? echo $rec["id_cont"] ?>/<? echo normalize_url($rec["titulo"]) ?>" title="" class="titulo"> <?=$rec["titulo"]?> </a>
                        <p class="color_grey">
                            <?=substr(strip_tags($rec['descripcion']),0,400)?>... [+]
                        </p>
<!--                        <a href="blog-post.php?id=<?=$rec["id_cont"]?>" class="btn">leer más</a> -->
                        <a href="/blog/<? echo $rec["id_cont"] ?>/<? echo normalize_url($rec["titulo"]) ?>" title="" class="btn"> leer más </a>

                    </div>

                <?php
                    $exist = true;
                }

                if (!$exist) {
                ?>

                    <div class="post">

                        <a href="blog-post.php?id=<?=$rec["id_cont"]?>" title="" class="titulo">Upsss!</a>
                        <p class="color_grey">
                            <strong>Lo sentimos pero no hemos encontrado resultados en esta categoría.</strong>

                            <br><br>
                            Si la categoría existe es porque estamos trabajando en publicaciones asociadas a ella. 
                            Si quieres, <strong>puedes suscribirte a nuestra newsletter</strong> para que te enviemos nuestro boletín periódicamente.
                            <br><br>
                        </p>

                        <div class="col_100 box_blog_list" style="padding:0px; margin:0px; margin-bottom:30px; background-color:transparent">
                            <div class="content left" style="padding:0px; margin:0px">
                                                                
                                O si lo prefieres, puedes seguir navegando por otras categorías:
                                <ul>

                                <?
                                $stmtC = $db->Query("SELECT * FROM t_categorias WHERE id_seccion=3 order by nombre asc"); 
                                while ($recC = $stmtC->fetch(PDO::FETCH_ASSOC)) { 
                                    $s_cat = $recC["nombre"];
                                ?>
                                    <li><a href="blog-categoria.php?c=<?=$recC["id"]?>" title=""><?= $s_cat ?></a></li>
                                <?
                                }
                                ?>

                                </ul>

                            </div>
                        </div>

                        <a href="blog-post.php?id=<?=$rec["id_cont"]?>" class="btn">Volver al blog</a>


                    </div>


                <?
                }


                ?>



                </div>

            </div>

            <div class="col_33">

                <!-- mailchimp newsletter
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/mailchimp-newsletter.php'); ?>


                <!-- blog buscador
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/blog-buscador.php'); ?>


                <!-- blog ultimas publicaciones
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/blog-ultimas.php'); ?>


                <!-- blog categorias
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/blog-categorias.php'); ?>


                <!-- edificio
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/edificio.php'); ?>


                <!-- servicios técnicos
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/servicios-tecnicos.php'); ?>


            </div>

        </div>


        <!-- footer
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/footer.php'); ?>


        <!-- copy
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/copy.php'); ?>


    </body>
</html>