<?
class Tools {
	static public function callStackTrace() {
        $dbt = debug_backtrace();
        $traza = "";
        foreach ($dbt as $line) {
            $args = "";
            if (is_array($line['args']))
                foreach ($line['args'] as $arg)
                    if (!is_object($arg))
                        $args .= "$arg,";
            if ($args != "") $args = substr($args, 0, strlen($args)-1);

            $traza .= "$line[file]:$line[line] $line[function]($args)\n";
        }

        return $traza;
	}

	static public function Microtime() {
	    list($usec, $sec) = explode(" ", microtime());
	    return ((float)$usec + (float)$sec);
	}

    public static function validateNumberFromString(&$number) {

        $value = "$number";
        $dot = substr_count($value, '.');
        $comma = substr_count($value, ',');

        // Ex: 100,12.12
        //if ($dot > 0 && $comma > 0) return false;
        // Asumimos que es 100.100,12
        if ($dot > 0 && $comma > 0) $value = str_replace(".", "", $value);

        // Replace , by .
        if ($comma > 0) $value = str_replace(",", ".", $value); 

        if (!is_numeric($value)) return false;

        if ($dot || $comma) $number = floatval($value);
        else $number = intval($value);

        return true;
    }

    public static function MysqlDateToNormal($date, $short_date=false) {
        if ($date == '') return '';
        // 2011-11-11 [12:12:12] => 11/11/2011 [12:12:12]
        $ret = substr($date, 8, 2)."/".substr($date, 5, 2)."/".substr($date, 0, 4);
        if (strlen($date) > 10) 
            $ret .= " ".substr($date, 11);

        if ($short_date) $ret = substr($ret, 0, 10);

        return $ret;
    }

    public static function boolToString($bool) {
        if ($bool) return "true";
        return "false";
    }

    public static function MysqlNumberToNormal($number) {
        $number = floatval($number);
        return number_format($number, 2, ",", ".");
    }

    public static function strToHex($string){
        $hex=' ';
        for ($i=0; $i < strlen($string); $i++){
            $hex .= str_pad(dechex(ord($string[$i])), 2, '0', STR_PAD_LEFT).' ';
        }
        return trim($hex);
    }    


}
?>