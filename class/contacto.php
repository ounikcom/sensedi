<?php include('inc/doctype.php'); ?>

<?php

    if ($_POST['nombre'] != '' && $_POST['comentarios'] != '') {

        // insertamos la consulta en la base de datos
        $db->Query("INSERT INTO t_contactos (id, fechahora, nombre, email, telefono, comentarios ) values(NULL, NOW(), ?, ?, ?, ?)", array($_POST['nombre'], $_POST['email'], $_POST['telefono'], $_POST['comentarios']));

        include('class/Email.php');
     
        // Enviamos email

        $email = new Email();
        $email->setDefaults();
        //$contacto_email = "david@piennsa.com";
        $contacto_email_destino = $contacto_email;
        $email->addAddress($contacto_email_destino);

        $email->Subject = "Contacto web";

        $body = "<table border=0 padding=5>";
        $body .= "<tr><td colspan=2><img src='assets/img/logo.png' width='130' alt='Sensedi' /></td></tr>";
        $body .= "<tr><td colspan=2></td><br/></tr>";
        $body .= "<tr><td colspan=2><b>Has recibido un contacto desde la web de Sensedi. Estos son los datos facilitados:</b></tr>";
        $body .= "<tr><td colspan=2><br/></td></tr>";
        $body .= "<tr bgcolor=#f0f0f0><td width='100px'><b>Nombre:</b></td><td>".$_POST['nombre']."</td></tr>";
        $body .= "<tr bgcolor=#f0f0f0><td width='100px'><b>Email:</b></td><td>".$_POST['email']."</td></tr>";
        $body .= "<tr bgcolor=#f0f0f0><td width='100px'><b>Teléfono:</b></td><td>".$_POST['telefono']."</td></tr>";
        $body .= "<tr bgcolor=#f0f0f0><td width='100px'><b>Comentarios:</b></td><td>".str_replace("\n", "<br>", $_POST['comentarios'])."</td></tr>";
        $body .= "<tr><td colspan=2><br/></td></tr>";
        $body .= "<tr><td colspan=2><b>Si lo deseas, también puedes acceder al backend para consultarla.</b></tr>";
        $body .= "<tr><td colspan=2>Este es un mensaje autómatico, no contestes a este correo.</td></tr>";
        $body .= "</table>";


        $email->MsgHTML($body);
        $email->send();

    }
?>
    <head>

        <!-- title (begin) -->
        <title><?=_I("head_title_contacto", "Sensedi")?></title>
        <!-- title (end) -->

        <!-- metas (begin) -->
        <meta name="description" content="<?=_I("head_metas_contacto_description", "")?>" />
        <meta name="keywords" content="<?=_I("head_metas_contacto_keywords", "")?>" />
        <?php include('inc/general-metas.php'); ?>
        <!-- metas (end) -->

        <!-- css (begin) -->
        <?php include('inc/css.php'); ?>
        <!-- css (end) -->

        <!-- favicon (begin) -->
        <?php include('inc/favicon.php'); ?>
        <!-- favicon (end) -->

        <!-- scripts (begin) -->
        <?php include('inc/scripts.php'); ?>

        <script>
        function chk_frm_qhdf() {

            if ($("#nombre").val() == "") {
                $("#frmOK").hide();
                $("#frmErr").show();
                $("#nombre").focus();
                return;
            }
            if ($("#telefono").val() == "") {
                $("#frmOK").hide();
                $("#frmErr").show();
                $("#telefono").focus();
                return;
            }
            if ($("#comentarios").val() == "") {
                $("#frmOK").hide();
                $("#frmErr").show();
                $("#comentarios").focus();
                return;
            }

            $("#frmErr").hide();
            $("#pepe").hide();
            $("#frmOK").show();
            $("#pepe").submit();
        }
        </script>        
        <!-- scripts (end) -->

        <!-- script google analytics (begin) -->
        <?php include('inc/google_analytics.php'); ?>
        <!-- script google analytics (end) -->

    </head>
    <body>

        <!-- menu (begin) -->
        <? include('inc/menu.php'); ?>
        <!-- menu (end) -->

        <!-- que hacemos (begin)-->
        <div class="row contacto">
            <div class="col_50" style="margin:0px">

                <div class="content back_white">
                    <div class="tit bold color_blue"><?=_I("body_contacto", "CONTÁCTANOS")?></div>                
                    <div class="txt color_grey">

                        <form id="pepe" method="post" action="contacto.php">
                            <label><?=_I("body_contacto", "NOMBRE")?></label>
                            <input type="text" name="nombre" id="nombre" />
                            <label><?=_I("body_contacto", "E-MAIL")?></label>
                            <input type="text" name="email" id="email" />
                            <label><?=_I("body_contacto", "TELÉFONO")?></label>
                            <input type="text" name="telefono" id="telefono" />
                            <label><?=_I("body_contacto", "COMENTARIOS")?></label>
                            <textarea name="comentarios" id="comentarios"></textarea>
                            <label class="foot">
                                <strong><?=_I("body_contacto", "Te damos respuesta en menos de 24 horas")?></strong><br/>
                                <?=_I("body_contacto", "Al enviar aceptas la <a href='proteccion-datos.php' target='_blank'>política de privacidad</a>")?>
                            </label>
                            <a class="btn" style="cursor:pointer" onclick="chk_frm_qhdf()"><?=_I("body_contacto", "ENVIAR")?></a>

                            <div id="frmErr" style="display:none"><font color="red"><?=_I("general", "Revisa los campos obligatorios por favor")?></font></div>

                        </form>
                        <div id="frmOK" style="display:none"><?=_I("general", "<strong>Tu consulta se ha enviado correctamente.</strong><br/>En las próximas horas nos pondremos en contacto contigo.<br/>Gracias.")?></div>
                    </div>
                </div>

            </div>
            <div class="col_50">
                <div class="col_100" id="box_location_int">
                    <div class="text center">
                        <span class="bold"><?=_I("body_contacto", "NOS PUEDES ENCONTRAR EN")?></span>
                        <br/>
                        <?=$contacto_direccion?><br/>
                        <?=$contacto_telefono?><br/>
                        <?=$contacto_email?>
                    </div>
                </div>
                <div class="col_100 box_in">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2993.592750428702!2d2.1567876151336622!3d41.3829319792645!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a2899a8623ad%3A0x2dd5cf8579ad4710!2sGran+Via+de+les+Corts+Catalanes%2C+533%2C+08011+Barcelona!5e0!3m2!1ses!2ses!4v1477039748702" width="100%" height="257" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="col_100 box_in back_extralightblue">
                    <div class="servicios_tecnicos center">
                        <div class="tit bold color_white"><?=_I("body_servicios_tecnicos", "SOLICITUD DE PRESUPUESTO")?></div>
                        <div class="txt color_white">
                            <?=_I("body_servicios_tecnicos", "Nuestro portafolio de Servicios Técnicos especializados aporta tranquilidad y transparencia a los propietarios de inmuebles a la vez que ayuda a planificar, controlar y reducir costes y a mejorar la calidad de los inmuebles.")?>
                        </div>
                        <a href="solicitud" class="btn"><?=_I("body_servicios_tecnicos", "SOLICITAR AHORA")?></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- que hacemos (end)-->

        <!-- footer (begin) -->
        <?php include('inc/footer.php'); ?>
        <!-- footer (end) -->

        <!-- copy (begin) -->
        <?php include('inc/copy.php'); ?>
        <!-- copy (end) -->


    </body>
</html>