<?php
/* 
    - Wrapper con default settings para el envío de emails.
*/

@include("PHPMailer/class.phpmailer.php");
@include("PHPMailer/class.smtp.php");

class Email extends PHPMailer {
	
	public $originalTo;
	public $externalId;
	
	function setDefaults() {

		$this->IsSMTP();
		$this->SMTPAuth = true;
		$this->SMTPSecure = "tls";
		$this->Host = "smtp.gmail.com";
		$this->Port = 587;
		//$this->SMTPDebug = 1;

		$this->Username = "ingenium.noreply@gmail.com";
		$this->Password = "noreply.ingenium"; // 01-01-2000, Otro
		$this->From = "ingenium.noreply@gmail.com";
		$this->FromName = "Ingenium - NoContestar";

		$this->CharSet = 'UTF-8';
		$this->IsHTML(true);	

	}

	function setExternalId($id) {
		$this->externalId = $id;
	}
	
	public function addAddress($address, $name = '') {
		$this->originalTo .= $address.", ";
		foreach (explode(";", $address) as $_email) parent::addAddress($_email);
		foreach (explode(",", $address) as $_email) parent::addAddress($_email);
	}

	function sendDeferred($obj)
	{	
		$serial = base64_encode(gzcompress(serialize($obj)));
				
		SQL("INSERT INTO emails (`to`, `body`, `externalId`) VALUES(?, ?, ?)", array($this->originalTo, $serial, $this->externalId));

		return true;
	}

	static function Cron_Emails_pendientes()
	{
	    $stmt = SQL("SELECT * from emails where `sent` IS NULL AND `replies` < 3 ORDER BY `created`");
	    while ($rec = $stmt->fetch(PDO::FETCH_ASSOC)) 
		{
			$email = unserialize(gzuncompress(base64_decode($rec["body"])));
			$email->setDefaults();

			if ($email->send()) {
				$stmtU = SQL("UPDATE emails SET `sent` = NOW(), msg = 'OK' WHERE id = ?", array($rec["id"]));
			} else {
				$stmtU = SQL("UPDATE emails SET replies = replies + 1, msg = ?, lastRetry = NOW() WHERE id = ?", array($email->ErrorInfo, $rec["id"]));
			} 
		}
		
	}	

	public function Show()
	{
		global $DB, $argv;

		$id1 = $argv[2];
		$id2 = $argv[3];
		$bemail = $argv[4];
		if ($id2 == '') $id2 = $id1;

		$stmt = SQL("SELECT id, body,replies from Emails where id BETWEEN $id1 AND $id2");
		echo "SELECT id, body,replies from Emails where id BETWEEN $id1 AND $id2\n";
		while ($rec = $stmt->fetch(PDO::FETCH_ASSOC))
		{
			$email = unserialize(gzuncompress(base64_decode($rec['body'])));
			if ($bemail != '' && $bemail != $email) continue;
			echo "ID: ".$rec['id']."\n";
			echo "From:".$email->From."\n";
			echo "To:\n";
			var_dump($email->all_recipients);
			echo "Subject:".$email->Subject."\n";
			echo "Body # ".$rec['id'].":".$email->Body."\n";
			echo "------------------------------------------------\n\n";
		}
		
	}	
}
?>