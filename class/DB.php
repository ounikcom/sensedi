<?
/* 
    - Define el funcionamiento de la Base de Datos. Usado principalmente en la clase Base
*/

class DB {
	private $connected;
	private $dbLink;
	private $error;
	public $lastSQL;
	private $dieOnError;
	private $duplicatedError = false;
	
	function __construct($host = '', $database = '', $user = '', $password = '') {
		$this->dieOnError = true;
		$this->connected = false;
		$this->error = 'NOT_CONNECTED';
		$this->lastSQL = '';

		if ($host != '' || $database != '' || $user != '' || $password != '') {
			return $this->Connect($host, $database, $user, $password);
		}

		return true;
	}

	public function Connect($host, $database, $user, $password) {
		return $this->_Connect($host, $database, $user, $password);
	}

	private function _Connect($host, $database, $user, $password) {
		$this->connected = false;
		$this->error = '';

		try {
			$this->lastSQL = 'CONNECT';
			$this->dbLink = new PDO('mysql:host='.$host.';dbname='.$database.';charset=utf8',
				$user,
				$password,
				array(
					PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
					PDO::ATTR_PERSISTENT => true,
    				PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
				)
			);
			$this->dbLink->exec("set names utf8");

			$this->connected = true;
		} catch (PDOException $ex) {
	        $this->error = "DB_CONNECT($host, $database, $user, $password): ".$ex->getMessage();
	        $this->error .= "\n".Tools::callStackTrace();

			// TODO: Log($this->error)

			$this->dieOnError();
		}

		return $this->connected;
	}

	public function getError() {
		return $this->error;
	}

	public function isDuplicatedRow() {
		return $this->duplicatedError;
	}

	private function dieOnError() {
		global $AJAX_QUERY;

		if (!$this->dieOnError || $this->error == '') return;
		if ($this->duplicatedError) return;

        if ($AJAX_QUERY) {
            $ret["error"] = str_replace("\n", "<br>", $this->error);
            die(json_encode($ret));
        }

		ob_start();
		echo "_GET\n";
		var_dump($_GET);
		echo "_POST\n";
		var_dump($_POST);
		echo "_SESSION\n";
		var_dump($_SESSION);
		$xtra = ob_get_clean();

		$message = $this->error."\n".$xtra;

		var_dump($message);

		if (php_sapi_name() === 'cli') {
			die($message);
		}

        die(str_replace("\n", "<br>", $message));
	} 
	
	public function Query($sql, array $params = null) {
		$this->error = '';
		$this->duplicatedError = false;
		if (!$this->connected) {
			return false;
		}

	    $start = Tools::Microtime();
    	$this->lastSQL = $this->sqlDebug($sql, $params);

	    try {
	        $stmt = $this->dbLink->prepare($sql);
	        if ($stmt->execute($params)) {
	            $elapsed = number_format(Tools::Microtime() - $start, 2);
	            $info = "SQL_OK ($elapsed s.)\n".$this->lastSQL;
	            // TODO: Log($info)
	        } else {
	            $elapsed = number_format(Tools::Microtime() - $start, 2);
	            $this->error = "SQL_ERROR ($elapsed s.) ".$stmt->errorInfo()."\n".$this->lastSQL."\n".Tools::callStackTrace();
	        }
	    } catch (PDOException $ex) {
	    	if (strpos($ex->getMessage(), "1062 Duplicate entry") !== false) {
	    		$this->duplicatedError = true;
	    	}

            $elapsed = number_format(Tools::Microtime() - $start, 2);
            $this->error = "SQL_ERROR ($elapsed s.) ".$ex->getMessage()."\n".$this->lastSQL."\n".Tools::callStackTrace();
	    }

		// TODO: Log($this->error)
		$this->dieOnError();
	    if ($this->error != '') return false;

	    return $stmt;
	}

	private function sqlDebug($sql_string, array $params = null) {

	    if (!empty($params)) {
	        $indexed = $params == array_values($params);
	        foreach($params as $k=>$v) {
	            if (is_object($v)) {
	                if ($v instanceof \DateTime) $v = $v->format('Y-m-d H:i:s');
	                else continue;
	            }
	            elseif (is_string($v)) $v="'$v'";
	            elseif ($v === null) $v='NULL';
	            elseif (is_array($v)) $v = implode(',', $v);

	            if (strlen($v) > 1000) {
	                $v = substr($v, 0, 1000);
	            }

	            if ($indexed) {
	                $sql_string = preg_replace('/\?/', $v, $sql_string, 1);
	            }
	            else {
	                if ($k[0] != ':') $k = ':'.$k; //add leading colon if it was left out
	                $sql_string = str_replace($k,$v,$sql_string);
	            }
	        }
	    }

	    return $sql_string;
	}

	public function LastInsertId() {
	    if (!$this->connected) {
	        return -1;
	    }

	    return $this->dbLink->lastInsertId();
	}

	public function Protect($v) {
		return $this->dbLink->quote($v);
	}

	public function NumRows() {
		return $this->stmt->rowCount();
	}

	public function ListEnumValues($table, $field){
		$stmt = $this->Query("SHOW COLUMNS FROM `$table` LIKE '$field'");
		$rec = $stmt->fetchObject();

		$rec->Type = str_replace("enum(", "", $rec->Type);
		$rec->Type = str_replace("ENUM(", "", $rec->Type);
		$rec->Type = substr($rec->Type, 0, strlen($rec->Type)-1);
		
		return str_getcsv($rec->Type , ',', "'");
	}
	
	public function ListFieldsForTable($table, $list=false, $exception=NULL) {
		$stmt = $this->Query("SHOW COLUMNS FROM `$table`");
		if (!$list) $ret = array();
		else $ret = '';		
		while ($rec = $stmt->fetchObject()) {
			$ignore = false;
			if (is_array($exception)) {
				foreach ($exception as $key)
					if ($rec->Field == $key) $ignore = true;
				if ($ignore) continue;
			}

			if (!$list) $ret[] = $rec->Field;
			else $ret .= ($ret == '' ? '' : ',').$rec->Field;		
		}
		
		return $ret;
	}
	
}

?>