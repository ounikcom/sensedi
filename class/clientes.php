<?php include('inc/doctype.php'); ?>
<?php

    $no_colaborador = 0;
    if ($_POST['user'] != '' && $_POST['psw'] != '') {
        $no_colaborador = 1;

        $user_in = $_POST['user'];
        $psw_in = md5($_POST['psw']);

        $user_logado = 0;
        $stmt = $db->Query("SELECT * FROM t_users where id_perms = 3 AND nombre = '$user_in' AND password = '$psw_in'"); 
        while ($rec = $stmt->fetch(PDO::FETCH_ASSOC)) { 
            $user_logado = 1;
        }

    }

?>
    <head>

        <!-- title
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <title><?=_I("head_title_clientes", "Sensedi")?></title>


        <!-- metas
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <meta name="description" content="<?=_I("head_metas_clientes_description", "")?>" />
        <meta name="keywords" content="<?=_I("head_metas_clientes_keywords", "")?>" />
        <?php include('inc/general-metas.php'); ?>


        <!-- css
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/css.php'); ?>


        <!-- favicon
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/favicon.php'); ?>


        <!-- scripts
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/scripts.php'); ?>

        <script type="text/javascript">

            function cargar_bip(){

                if ($("#ref_catastral").val() != '') {

                    $('#intro_bip').slideToggle('fast');
                    $('#bip').slideToggle('fast');
                    
                    var ref = $("#ref_catastral").val();
                    $("#iframe_bip").prop("src", "http://62.97.116.174:3333/viewer.html?refcat="+ref);

                } else {

                    alert('<?=_I("clientes_BIP", "Debes introducir una referencia catastral")?>');
                    return;

                }
            }     

            function chk_frm_qhdf() {

                if ($("#user").val() == "") {
                    $("#frmOK").hide();
                    $("#frmErr").show();
                    $("#user").focus();
                    return;
                }
                if ($("#psw").val() == "") {
                    $("#frmOK").hide();
                    $("#frmErr").show();
                    $("#psw").focus();
                    return;
                }
                $("#pepe").submit();

                $("#frmErr").hide();
                $("#pepe").hide();
                $("#frmOK").show();

            }

        </script>


        <!-- script google analytics
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/google_analytics.php'); ?>


    </head>
    <body>

        <!-- menu
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <? include('inc/menu.php'); ?>


        <!-- clientes
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <div class="row">
            <div class="col_100" id="area_clientes">
                <div class="box">
                    <div class="tit bold"><?=_I("body_clientes", "ÁREA CLIENTES")?></div>
                    <div class="img"><img src="assets/img/imago-blanco.png" alt="Sensedi" /></div>
                    <form id="pepe" method="post" action="clientes.php">
                        <div class="frm">
                            <input type="text" name="user" id="user" placeholder="<?=_I("general", "nombre de usuario")?>" />
                            <input type="password" name="psw" id="psw" placeholder="<?=_I("general", "contraseña")?>" />
                        </div>
                        <a href="#" class="btn" onclick="chk_frm_qhdf()"><?=_I("body_clientes", "LOGIN")?></a>
                        <div id="frmErr" style="display:none"><?=_I("general", "Revisa los campos obligatorios por favor")?></div>
                        <div id="nohay" style="display:none"><?=_I("general", "No existen los datos introducidos")?></div>
                    </form>                    
                </div>
            </div>
            <div class="col_100" id="area_clientes-in" style="display:none;">

                <div id="intro_bip">
                    <div class="txt">
                        <span><?=_I("clientes_BIP", "Bienvenido a BIP<br/><strong>Si tienes una REFERENCIA CATASTRAL, introducela aquí para cargar el BIP.</strong><br/>En caso de no tener ninguna, ponte en contacto con nosotros para que podamos facilitártela.")?></span>
                    </div>
                    <div class="casilla">
                        <input type="text" name="ref_catastral" id="ref_catastral" placeholder="<?=_I("clientes_BIP", "Referencia catastral")?>" />
                        <a href="#" class="btn" onclick="cargar_bip()"><?=_I("clientes_BIP", "CARGAR BIP")?></a>
                    </div>
                </div>

                <div id="bip" style="display:none;">
                    <?php
                        $ref = $_GET["ref"];
                       // $ref_bip = "8225602DF2882E";
                    ?>
                    <iframe id="iframe_bip" width="100%" height="600" src="" frameborder="0" allowfullscreen></iframe>
                </div>

            </div>
        </div>


        <!-- footer
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/footer.php'); ?>


        <!-- copy
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/copy.php'); ?>


        <?php if ( $user_logado == 1) { ?>
            <script type="text/javascript">
                $('#area_clientes').slideToggle('fast');
                $('#area_clientes-in').slideToggle('fast');
            </script>
        <?php } ?>

        <?php if ($no_colaborador == 1) { ?>
            <script type="text/javascript">
                $('#nohay').slideToggle('fast');    
            </script>

        <?php } ?>

    </body>
</html>