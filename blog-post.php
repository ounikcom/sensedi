<?php include('inc/doctype.php'); ?>
<?php include("inc/wordpress.php"); ?>

    <?

    $strid = $_GET['id'];

    $query = "SELECT ID, post_title, post_content
            FROM wp_posts WHERE post_status = 'publish' and ID = $strid";

    $result = $db_wp->Query($query); 
    while ($rst = $result->fetch(PDO::FETCH_ASSOC)) { 

        $ruta_imagen = "";

        $query1 = "select meta_value
                from wp_postmeta where meta_key='_thumbnail_id' and post_id = " .$rst['ID'];
        $result1 = $db_wp->Query($query1); 
        $rst1 = $result1->fetch(PDO::FETCH_ASSOC);

        if ($rst1 != "") {

            $query2 = "select meta_value
                    from wp_postmeta where meta_key='_wp_attached_file' and post_id = " .$rst1['meta_value'];
            $result2 = $db_wp->Query($query2); 
            $rst2 = $result2->fetch(PDO::FETCH_ASSOC);

            $ruta_imagen = "http://www.sensedi.com/admin/wp-content/uploads/".$rst2['meta_value'];
        }


        $str_titulo = $rst['post_title'];
        $description = str_replace("\n", "<br>", $rst['post_content']);
        $description = str_replace('"',"'", $description);
    }
    ?>

    <head>

        <!-- title
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <title><?= $str_titulo ?></title>


        <!-- metas
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <meta name="description" content="<?= $description ?>" />
        <meta name="keywords" content="<?=_I("head_metas_contacto_keywords", "")?>" />
        <?php include('inc/general-metas.php'); ?>


        <!-- css
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/css.php'); ?>


        <!-- favicon
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/favicon.php'); ?>


        <!-- scripts
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/scripts.php'); ?>


        <!-- script google analytics
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/google_analytics.php'); ?>


        <!-- popup mailchimp suscriber
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/mailchimp-popup-suscriber.php'); ?>


    </head>
    <body>

        <!-- menu
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <? include('inc/menu.php'); ?>


        <!-- blog post
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <div class="row blog">

            <div class="col_66" style="margin:0px">

                <div class="content back_white">


                    <div class="post">

                        <? if ($ruta_imagen != "") { ?>
                        <div class="img_post" style="background-image: url('<?=$ruta_imagen?>');"></div>
                        <? } ?>
                        <h1><?= $str_titulo ?></h1>

                        <div class="etiquetas">

                            <?
                            $query_post_term = "select term_taxonomy_id from wp_term_relationships where object_id = ".$strid;

                            $result_post_term = $db_wp->Query($query_post_term); 
                            while ($rst_post_term = $result_post_term->fetch(PDO::FETCH_ASSOC)) { 

                                if ($rst_post_term['term_taxonomy_id'] != "1") {
                                    $query_term_item = "select * from wp_terms where term_id = ".$rst_post_term['term_taxonomy_id'];
                                    $result_term_item = $db_wp->Query($query_term_item); 
                                    $rst_term_item = $result_term_item->fetch(PDO::FETCH_ASSOC);
                                    echo "<a href='/etiquetas/".$rst_term_item['term_id']."/".normalize_url($rst_term_item['name'])."' title='".$rst_term_item['name']."'>".$rst_term_item['name']."</a>";
                                }

                            }
                            ?>
                        
                        </div>


                        <p class="color_grey">
                            <?= $description ?>
                        </p>

                        <div class="shared">
                            <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=591c20d177132c00117cb2dd&product=inline-share-buttons"></script>
                            <div class="sharethis-inline-share-buttons"></div>
                        </div>

                        <a href="/blog" class="btn" style="margin-top:50px"> < <?=_I("blog", "volver al listado")?></a>


                    </div>

                </div>

            </div>

            <div class="col_33">

                <!-- blog ultimas publicaciones
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/blog-ultimas.php'); ?>


                <!-- edificio
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/edificio.php'); ?>


                <!-- blog categorias
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
                <?php include('inc/servicios-tecnicos.php'); ?>


            </div>

        </div>


        <!-- footer
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/footer.php'); ?>


        <!-- copy
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/copy.php'); ?>


    </body>
</html>