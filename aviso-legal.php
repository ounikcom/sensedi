<?php include('inc/doctype.php'); ?>
    <head>

        <!-- title
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <title><?=_I("head_title_index", "Servicios Técnicos, Periciales y Mantenimiento de Inmuebles")?></title>


        <!-- metas
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <meta name="description" content="<?=_I("head_metas_home_description", "En Sensedi ofrecemos las mejores soluciones para servicios técnicos, periciales y mantenimiento de inmuebles, usando la más avanzada tecnología e innovación.")?>" />
        <meta name="keywords" content="<?=_I("head_metas_home_keywords", "Servicios técnicos, servicios periciales, mantenimiento de inmuebles, mantenimiento de edificios, rehabilitación de edificios, reforma de edificios, rehabilitación de viviendas")?>" />
        <?php include('inc/general-metas.php'); ?>


        <!-- css
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/css.php'); ?>


        <!-- favicon
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/favicon.php'); ?>


        <!-- scripts
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/scripts.php'); ?>


        <!-- script google analytics
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/google_analytics.php'); ?>


        <!-- popup mailchimp suscriber
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/mailchimp-popup-suscriber.php'); ?>


    </head>
    <body>

        <!-- menu
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <? include('inc/menu.php'); ?>


        <!-- solicitud servicios
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <div class="row">
            <div class="col_100 center back_white" id="solicitud_servicios" style="padding-bottom:100px">
                <h1 class="bold back_white" style="padding-bottom:40px;"><?= _I("footer_footer", "Aviso legal")?></h1>
        
                <div class="descripcion color_grey" style="text-align: left;">
                    
                    <? if ($_SESSION['idioma_code']=='es') { ?>

                        El objeto de la información contenida en esta página web, es la de presentar nuestra compañía e informar de los productos y servicios que presta, a todas aquellas personas o entidades interesadas en su conocimiento.
                        <br/><br/>
                        <strong>SENSEDI</strong> se reserva el derecho de realizar, sin necesidad de aviso previo, cualquier modificación, tanto de la información como de la presentación contenida en esta Web, que considere oportuna.
                        <br/><br/>
                        SENSEDI no se responsabilizará de las consecuencias, daños o perjuicios que se deriven de la navegación por esta Web y del uso de la información contenida en ella. Así mismo, no implica respaldo por parte de nuestra entidad, a los productos o servicios de terceros a los que se pueda hacer referencia ocasionalmente.
                        <br/><br/>
                        No podrá realizarse reproducción, copia o distribución de toda o parte de la información contenida en esta página web, sin autorización expresa de <strong>Sensedi</strong>. 

                    <? } elseif ($_SESSION['idioma_code']=='ca') { ?>

                        L'objecte de la informació continguda en aquesta pàgina web, és la de presentar la nostra companyia i informar dels productes i serveis que presta, a totes aquelles persones o entitats interessades en el seu coneixement.
                        <br/><br/>
                        <strong>SENSEDI</strong> es reserva el dret de realitzar, sense necessitat d'avís previ, qualsevol modificació, tant de la informació com de la presentació continguda en aquesta web, que consideri oportuna.
                        <br/><br/>
                        SENSEDI no es responsabilitzarà de les conseqüències, danys o perjudicis que es derivin de la navegació per aquesta web i de l'ús de la informació continguda en ella. Així mateix, no implica cap suport per part de la nostra entitat, als productes o serveis de tercers als quals es pugui fer referència ocasionalment.
                        <br/><br/>
                        No podrà realitzar reproducció, còpia o distribució de tota o part de la informació continguda en aquesta pàgina web, sense autorització expressa de <strong>Sensedi</strong>.

                    <? } elseif ($_SESSION['idioma_code']=='en') { ?>

                        The object of the information contained in this website is to present our Company and information on the products and services provided to individual or entities who may be interested. 
                        <br/><br/>
                        <strong>SENSEDI</strong> reserves the right to make any modification on both the information and presentation which deems appropriate in this website without prior notice.
                        <br/><br/>
                        SENSEDI is not responsible for the consequences, damage or loss that may result from browsing this website and from the use of the information contained therein. The reference to products and services of third parties on our website does not imply endorsement by our entity. 
                        <br/><br/>
                        No reproduction, copy or distribution of all or part of the information contained on this website is allowed without express authorisation of <strong>Sensedi</strong>.

                    <? } elseif ($_SESSION['idioma_code']=='fr') { ?>

                        Les informations mentionnées sur cette page web ont pour but de présenter notre entreprise et de donner des informations sur les produits et les services que nous proposons, à toute personne ou entité désireuse d'en prendre connaissance.
                        <br/><br/>
                        <strong>SENSEDI</strong> se réserve le droit de modifier sans préavis ces informations, ainsi que leur présentation sur ces pages web, de la façon qui lui paraîtra pertinente.
                        <br/><br/>
                        SENSEDI décline toute responsabilité concernant les dommages ou préjudices qui résulteraient de la consultation de ce site web et de l'utilisation des informations qui y sont mentionnées. De même, celles-ci n'impliquent aucun soutien de notre entité aux produits et aux services de tiers, auxquels il peut être fait occasionnellement référence. 
                        <br/><br/>
                        Il est interdit de reproduire, copier ou distribuer tout ou partie des informations mentionnées sur ces pages, sans l'autorisation expresse d'<strong>Sensedi</strong>.

                    <? } ?>

                </div>
            </div>
        </div>


        <div class="row subline"></div>


        <!-- footer
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/footer.php'); ?>


        <!-- copy
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
        <?php include('inc/copy.php'); ?>


    </body>
</html>